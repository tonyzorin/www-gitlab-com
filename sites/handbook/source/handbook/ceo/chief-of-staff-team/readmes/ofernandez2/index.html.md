---
layout: markdown_page
title: "Omar's README"
---

<!-- This template will help you build out your very own GitLab README, a great tool for transparently letting others know what it's like to work with you, and how you prefer to be communicated with. Each section is optional. You can remove those you aren't comfortable filling out, and add sections that are germane to you. --> 

## Omar's README

**[Omar Fernandez, Director of Strategy and Operations]** 

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## Related pages

[GitLab](https://gitlab.com/ofernandez2)
[Personal blog](https://omareduardo.com/)
[LinkedIn profile](https://www.linkedin.com/in/omareduardo/)

## About me

* Born and raised in Mayagüez, Puerto Rico, I love visiting my family, most of which lives in the West Coast of Puerto Rico. 
* I like to learn by doing. After studying Chemical Engineering in undergrad, I wanted to learn about business so I joined a consulting company to do so. To learn about tech, I joined Bloomreach in customer success, later on transitioning into Product Management. Learning on the job while being accountable for RESULTS has been hugely rewarding for me, far more so than theoretical classroom learning. 
* I love to read books and track my reading in [my Goodreads profile](https://www.goodreads.com/user/show/132507049-omar-fern-ndez)
* My husband and I live in Brooklyn, NY since late 2019. Given the COVID pandemic, which started around the same time, my NYC experience has been unique and full of unique charm. 

## How you can help me

* I value constructive feedback. The best way to learn is by hearing from others what I'm doing well and the areas where I can improve. Please don't shy away from giving me your feedback. If you work at GitLab and would like to give me feedback anonymously, you can use [this form](https://docs.google.com/forms/d/e/1FAIpQLSfLDiGotjcQsbM-jH9vVTH1lVWKjZgwj3raZ8gcUiFN3LwuaQ/viewform) to do so. 

## My working style

* I value clear, direct communication. I'm very open and honest, although I care about others and aim to deliver difficult messages with tact. 
* When inviting me to a meeting, please make it clear in the invite or meeting agenda why my participation is important. This helps me manage my time appropriately among competing priorities. 
* Please try to send meeting invites 2+ days ahead of when you'd like to meet, unless we've agreed to otherwise. It helps me better plan my work and schedule ahead of time. 
* When you message me via email, Slack message, or tag me in a working document or issue, I will try to get back to you within 1 working day. I strive for "Inbox zero" across main communications channels and stick pretty close to it. If I haven't responded to you within 1-2 working days, don't hesitate to ping again for important matters, it may just be an atypically busy time. 
* I value my time "off" work and aim to not check any work messages outside of my working hours. 

## What I assume about others

* I assume that we'll follow-through on anything that we've agreed to do, unless either of us explicitly communicates a change in priorities.
* I'll assume honesty and good intent in our interactions. 

## What I want to earn

* My goal at GitLab is to contribute to the best of my abilities and, as I do so, to learn and get better each day. 
* It's not all work, I highly value connection and developing good relationships with my coworkers. There's nothing better than being able to genuinely laugh with colleagues as we navigate both the ups and downs at work and in life. 

## Strengths/Weaknesses

* I like to drive results, which is generally a strength, but at times it makes me focus on the first path to a solution and not explore enough alternatives. If you notice that I'm jumping to a solution before exploring alternatives, please tell me. 
* My energy level and mood get impacted very noticeably by hunger or a lack of sleep. If we have a meeting and I wasn't able to sleep well or grab a meal, I will aim to let you know so you're not surprised by unusually low energy. 
