---
layout: handbook-page-toc
title: "Sarbanes-Oxley (SOX) Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Purpose

To develop a formalized system of checks and balances, thereby helping protect GitLab stakeholders from fraudulant financial reporting.
<br>

## Scope

Currently [ Sarbanes- Oxley (SOX) ](https://en.wikipedia.org/wiki/Sarbanes%E2%80%93Oxley_Act) regulations do not apply to GitLab. As part of GitLab's [public company readiness](https://about.gitlab.com/handbook/being-a-public-company/#public-company-readiness) milestone, we are in the process of implementing a SOX program.  

GitLab has adopted the [COSO framework](https://www.coso.org/Pages/ic.aspx) as the criteria for evaluating the effectiveness of the company’s internal control over financial reporting.

## Roles and Responsibilities


|Role | Responsibility|
|:------|:-----------|
|SOX Progam Management Office (PMO)|The division of [internal audit](https://about.gitlab.com/handbook/internal-audit/) department has the primary responsibility of managing GitLab’s Sarbanes-Oxley (SOX) program. In this role, the PMO will work under the direction of the Principal Accounting Officer. Responsibilities include: <br> &#8594; Perform risk assesment and scoping to determine project scope of each reporting year  <br> &#8594; Prepare the [internal control assessment](https://internal-handbook.gitlab.io/handbook/finance/sox-internal-controls/control-assessment-procedure/) plan and include timelines <br> &#8594; Schedule process walk-throughs for each process with process/control owners <br> &#8594; Review current and prior-year control deficiencies in order to determine the remediation status <br> &#8594; Update process maps<br> &#8594; Prepare control deficiency reports and follow up on remediation efforts <br> &#8594; Meet with external auditors as necessary to provide status updates and remediation efforts of ongoing work <br> |
|Principal Accounting Officer|Executive Sponsor and provides oversight of Internal Audit’s execution of the SOX program|
|Director, SEC & SOX| Responsible for: <br> &#8594; Confirming control description for controls that are assigned to them <br> &#8594; Update [SOX Internal controls page](https://internal-handbook.gitlab.io/handbook/finance/sox-internal-controls/) for any changes in the people, process and platform and provide edits to the SOX PMO for review within 30 days of change<br> &#8594; Make themselves available to speak with the SOX PMO upon their request for walkthroughs, sign-offs , testing and for discussion of test results<br> &#8594; Provide test evidence to the SOX PMO upon request.<br> &#8594; Remediate control deficiencies|

## Outputs of the SOX Program

Following outputs of SOX program will be maintained and monitored by the Internal Audit function.

- Risk Assessment and Scoping documentation
- Controls testing plan
- Risk Control Matrix
- [Control observations and remediation](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/observation-management.html) plan tracker
































