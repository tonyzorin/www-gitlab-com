---
layout: job_family_page
title: "Tax Accounting and Domestic Tax"
---

## Levels

### Director, Tax Accounting and Domestic Tax

The Director, Tax Accounting and Domestic Tax report to the VP Tax and will interact with one peer and several Tax Department members.

### Director, Tax Accounting and Domestic Tax Job Grade

The Director of Tax Accounting and Domestic Tax is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Director, Tax Accounting and Domestic Tax Responsibilities

* Staff mentoring and supervision
* SOX control implementation, testing, and narrative writing
* Process implementation for global income tax accounting
* Interaction with Big 4 financial statement auditors
* Working with outside service providers on projects including but not limited to:
* Domestic income tax compliance
* Sales tax compliance
* Maintenance of a domestic compliance calendar
* Domestic tax audit supervision, both direct and indirect taxes
* M&A and purchase accounting (income and indirect taxes)
* Tax account reconciliation
* Working with Finance team members on intercompany settlements

### Director, Tax Accounting and Domestic Tax Requirements

* Bachelor’s Degree (B.S.) in Accounting. Master’s Degree in Business Taxation preferred.
* JD and/or CPA preferred.
* Experience with Software and/or SAAS in a high growth environment.
* Technical understanding of ASC 740, ASC 718, and ASC 805 for global operations
* Spreadsheet modeling for ASC 740, tax return workpapers, and project planning
* Tax and tax provision issues relating to stock based compensation, including contra-DTAs produced by Sec. 162M
* Some working knowledge of VAT/GST, transfer pricing, and US international taxes including FDII, GILTI, BEAT, FTCs and Subpart F
* Project management
* Audit supervision
* Memo writing
* Taking initiative and also working as a team player
* Experience with NetSuite, SalesForce, Zuora, Coupa, GSuite and Avalara are plusses.
* Ability to use GitLab

### Performance Indicators

* Timely, complete, and accurate completion of tax accounting
* Timely, complete, and accurate completion of tax returns
* Efficient use of company resources
* Project management (successful drive projects to completion, regardless of the resource chosen)
* Technical abilities

### Career Ladder

The next step in the Director, Tax Accounting and Domestic Tax job family is Sr. Director within the Tax Department.

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a first interview with our VP, Tax
* Candidates will then be invited to schedule an interview with our Director, Tax
* Next, candidates will be invited to schedule an interview with our VP, Corporate Controller
* Finally, candidates will have a 30min call with either our CFO

Additional details about our process can be found on our [hiring page](/handbook/hiring)
